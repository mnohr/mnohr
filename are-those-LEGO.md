# Are those LEGO?

If you've had a video call with me, you've probably seen some shelves behind me with miniature buildings. I often get asked "Are those LEGO?" The answer is YES!  

Side note, remember it is [LEGO not Legos](https://legonotlegos.com). I'm not that picky, and you can say it however you want, but I'll always say LEGO.  

## LEGO Modular Buildings  

In 2007, the LEGO group started a new series of sets aimed at adults (AFOL = Adult Fan of Lego). Here is a brief description from [Wikipedia](https://en.wikipedia.org/wiki/Lego_Modular_Buildings):  

> The sets in this series are generally intended for more advanced builders. Most sets contain more than 2,000 total pieces and make use of unorthodox building techniques not usually used in previous official Lego sets. In contrast to most Lego sets aimed at children and adolescents, the suggested age of most sets in the Modular Buildings series is 16 years or older. The Modular Buildings sets have been received with positive reviews and are considered by Lego designers and fans as "toys for adults".  

A new set comes out every year. Each set is only available for a few years and then LEGO "retires" them and they are no longer for sale. You can still buy them from [3rd-party sellers](https://www.bricklink.com/catalogList.asp?catType=S&catString=171.1068.710) but they can get quite expensive. I've seen them sell for up to five times their original price.  

The buildings are generally around 12" tall (30cm), with the Fire Brigade being the tallest one I have over 15" (38 cm). They are mostly 10" wide (25cm), which is 32 studs, which is the size of the standard large base plate. The Assembly Square set was the 10th anniversary set and it is the widest at 48 studs wide.  

All the buildings are designed to snap together. There are a few corner buildings as well with the intention that you could make a city block.

## My Collection  

In 2009 I bought my first one. It was the Fire Brigade, which had just come out. I was getting back into LEGO sets after loving building with them as a kid. Since then I've gotten them as gifts or just bought them myself.  

There are currently (as of 2024) 19 sets in the series, and I have 13 of them. Here are all the ones I have:  

| Year | Name                | Picture                                                                                                                                |
|------|---------------------|----------------------------------------------------------------------------------------------------------------------------------------|
| 2009 | Fire Brigade        | <a href="https://mnohr.gitlab.io/mnohr/lego/10197-1.png"><img src="https://mnohr.gitlab.io/mnohr/lego/10197-1.png" height="100" /></a> |
| 2010 | Grand Emporium      | <a href="https://mnohr.gitlab.io/mnohr/lego/10211-1.png"><img src="https://mnohr.gitlab.io/mnohr/lego/10211-1.png" height="100" /></a> |
| 2011 | Pet Shop            | <a href="https://mnohr.gitlab.io/mnohr/lego/10218-1.png"><img src="https://mnohr.gitlab.io/mnohr/lego/10218-1.png" height="100" /></a> |
| 2013 | Palace Cinema       | <a href="https://mnohr.gitlab.io/mnohr/lego/10232-1.png"><img src="https://mnohr.gitlab.io/mnohr/lego/10232-1.png" height="100" /></a> |
| 2014 | Parisian Restaurant | <a href="https://mnohr.gitlab.io/mnohr/lego/10243-1.png"><img src="https://mnohr.gitlab.io/mnohr/lego/10243-1.png" height="100" /></a> |
| 2015 | Detective’s Office  | <a href="https://mnohr.gitlab.io/mnohr/lego/10246-1.png"><img src="https://mnohr.gitlab.io/mnohr/lego/10246-1.png" height="100" /></a> |
| 2016 | Brick Bank          | <a href="https://mnohr.gitlab.io/mnohr/lego/10251-1.png"><img src="https://mnohr.gitlab.io/mnohr/lego/10251-1.png" height="100" /></a> |
| 2017 | Assembly Square     | <a href="https://mnohr.gitlab.io/mnohr/lego/10255-1.png"><img src="https://mnohr.gitlab.io/mnohr/lego/10255-1.png" height="100" /></a> |
| 2019 | Corner Garage       | <a href="https://mnohr.gitlab.io/mnohr/lego/10264-1.png"><img src="https://mnohr.gitlab.io/mnohr/lego/10264-1.png" height="100" /></a> |
| 2020 | Bookshop            | <a href="https://mnohr.gitlab.io/mnohr/lego/10270-1.png"><img src="https://mnohr.gitlab.io/mnohr/lego/10270-1.png" height="100" /></a> |
| 2021 | Police Station      | <a href="https://mnohr.gitlab.io/mnohr/lego/10278-1.png"><img src="https://mnohr.gitlab.io/mnohr/lego/10278-1.png" height="100" /></a> |
| 2022 | Boutique Hotel      | <a href="https://mnohr.gitlab.io/mnohr/lego/10297-1.png"><img src="https://mnohr.gitlab.io/mnohr/lego/10297-1.png" height="100" /></a> |
| 2023 | Jazz Club           | <a href="https://mnohr.gitlab.io/mnohr/lego/10311-1.png"><img src="https://mnohr.gitlab.io/mnohr/lego/10312-1.png" height="100" /></a> |

### Why don't I have them all?  

When I bought the first one, I didn't think I would end up collecting them. So, even though they were available, I did not get the first 3 in the series at that time. These were the Cafe Corner, Market Street, and Green Grocer. I also missed out on the Town Hall in 2012 because new it was one of the most expensive and currently sells second-hand for 2x-3x the original price.  

Then there is the Downtown Diner. I really debated if I should get that one because by the time it came out in 2018 I had a good number of the sets. But I really hate how it looks so I never got it.  