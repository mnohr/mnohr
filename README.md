<img src="https://mnohr.gitlab.io/mnohr/gitlab-logo-100.png" title="GitLab" height="40" />

[Backend Engineering Manager](https://about.gitlab.com/job-families/engineering/backend-engineer/#backend-engineering-manager)

- [Create:Code Creation Team Page](https://about.gitlab.com/handbook/engineering/development/dev/create/code-creation/)
- [Code Suggestions Documentation](https://docs.gitlab.com/ee/user/project/repository/code_suggestions/)
- [Code Suggestions Direction](https://about.gitlab.com/direction/create/code_creation/code_suggestions/)

My GitLab Helper Apps: [Milestone Dates](https://mnohr.gitlab.io/milestone-dates/) | [Markdown Table Formatter](https://mnohr.gitlab.io/markdown-table-formatter/) | [Metric Conversion](https://simple-unit-conversion-9d4cef.gitlab.io/)